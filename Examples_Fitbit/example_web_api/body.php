<?php
    require("vendor/autoload.php");
    use djchen\OAuth2\Client\Provider\Fitbit;

    session_start();

    if(isset($_SESSION['auth_token'])){
          
        $auth_token = $_SESSION['auth_token'];

        if(isset($_SESSION['access_token'])){

            $access_token = unserialize($_SESSION['access_token']);
    
            if($access_token->hasExpired()){
                //Refresh access token
                echo "<a href=create_request.php?request_url=${request_url}&access_token=${access_token}>
                    <button type='button' class='btn btn-primary'>Refresh Access Token</button>
                    </a>";
            }
    
            /* Make a simple request by asking the user profile. For more info about other requests, 
               visit https://dev.fitbit.com/build/reference/web-api/ */

            //$request_url = '/1/user/-/activities/heart/date/today/1d.json'; 
            $request_url = '/1/user/-/profile.json';
    
            echo "<a href=create_get_request.php?request_url=${request_url}>
                    <button type='button' class='btn btn-primary'>Request Example</button>
                </a>";
  
        } else {
            //Request an access token
            echo "<a href=use_league.php?code=${auth_token}>
                    <button type='button' class='btn btn-primary'>Get Access Token</button>
                  </a>";
        }
    } else {
        //Request an auth token
        echo "<a href=use_league.php>
                <button type='button' class='btn btn-primary'>Get Authorization Token</button>
              </a>";
    }      
?>