<?php
    require("vendor/autoload.php");
    use djchen\OAuth2\Client\Provider\Fitbit;

    $client_id = '{client_id}';
    $client_secret = '{client_secret}';
    $redirect_uri = '{redirect_uri}';

    $provider = new Fitbit([
        'clientId'          => $client_id,
        'clientSecret'      => $client_secret,
        'redirectUri'       => $redirect_uri
    ]);

    // start the session
    session_start();

    // If we don't have an authorization code then get one
    if (!isset($_GET['code'])) {

        // Fetch the authorization URL from the provider; this returns the
        // urlAuthorize option and generates and applies any necessary parameters
        // (e.g. state).
        $authorizationUrl = $provider->getAuthorizationUrl();

        // Redirect the user to the authorization URL.
        header('Location: ' . $authorizationUrl);
        exit;

    //Check given state against previously stored one to mitigate CSRF attack
    } else {

        try {

            // Try to get an access token using the authorization code grant.
            $accessToken = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);

            // On success, store the access token for future authenticated requests
            if(isset($accessToken)){
                $_SESSION['access_token'] = serialize($accessToken);
                header('Location: index.php');
            }
            

        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

            // Failed to get the access token or user details.
            exit($e->getMessage());

        }

    }
?>