In questo esempio ci si è serviti della libreria "djchen/oauth-fitbit" per l'implementazione del protocollo OAuth2.0
per info: https://oauth2-client.thephpleague.com/ e https://github.com/djchen/oauth2-fitbit

Prima di cominciare:
1. Aprire un canale ngrok che esponga all'esterno localhost
2. Registrare un'app client su https://dev.fitbit.com/apps/new
3. Durante la registrazione, inserire nel campo 'redirect uri' canalengrok/example_web_api/receive_auth_token.php
4. Compilare nei vari file i campi "client_id", "client_secret" e "redirect_uri" con quelli forniti da fitbit

N.B: questo programma vuole fornire un semplice esempio del flow delle informazioni e chiamate ad API e non è da
     considerare una base sicura per sviluppare un'applicazione robusta che si integri con il web server Fitbit.