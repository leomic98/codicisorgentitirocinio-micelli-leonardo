<?php
    require("vendor/autoload.php");
    use djchen\OAuth2\Client\Provider\Fitbit;

    $client_id = '{client_id}';
    $client_secret = '{client_secret}';
    $redirect_uri = '{redirect_uri}';

    $provider = new Fitbit([
        'clientId'          => $client_id,
        'clientSecret'      => $client_secret,
        'redirectUri'       => $redirect_uri
    ]);

    session_start();

    $existingAccessToken = $_SESSION['access_token'];
    $existingRefreshToken = $_SESSION['refresh_token'];

    if ($existingAccessToken->hasExpired()) {
        $newAccessToken = $provider->getAccessToken('refresh_token', [
            'refresh_token' => $existingAccessToken->getRefreshToken()
        ]);
    
        $_SESSION['access_token'] = $newAccessToken->getToken();
    }

?>