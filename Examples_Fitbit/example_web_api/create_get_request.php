<?php
    require("vendor/autoload.php");
    use djchen\OAuth2\Client\Provider\Fitbit;

    $client_id = '{client_id}';
    $client_secret = '{client_secret}';
    $redirect_uri = '{redirect_uri}';

    $provider = new Fitbit([
        'clientId'          => $client_id,
        'clientSecret'      => $client_secret,
        'redirectUri'       => $redirect_uri
    ]);

    session_start();

    $request_url = $_GET['request_url'];
    $access_token = unserialize($_SESSION['access_token']);

    try {

        $request = $provider->getAuthenticatedRequest(
            Fitbit::METHOD_GET,
            Fitbit::BASE_FITBIT_API_URL . $request_url,
            $access_token->getToken(),
            ['headers' => [Fitbit::HEADER_ACCEPT_LANG => 'en_US'], [Fitbit::HEADER_ACCEPT_LOCALE => 'en_US']]
        );
        // Make the authenticated API request and get the parsed response.
        $response = $provider->getParsedResponse($request);
        //Show response
        var_dump($response);

    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

        // Failed to get the access token or user details.
        exit($e->getMessage());

    }

?>