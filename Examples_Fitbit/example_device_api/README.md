Per eseguire il progetto occorre avere installato:
1. Fitbit OS Simulator (installers su https://dev.fitbit.com/getting-started/ sezione "What you'll need")
2. Account fitbit

Installazione ed esecuzione con Fitbit Studio:
1. Accedere col proprio account Fitbit a https://studio.fitbit.com/
2. Creare un nuovo progetto, dopo la creazione, è sufficiente copiare ed incollare i files in questa directory
   direttamente nell'explorer
3. Collegare Fitbit OS Simulator (o un device Fitbit se lo si possiede) all'IDE
4. Buildare ed installare il progetto

Installazione ed esecuzione con CLI:
È possibile installare ed eseguire il progetto da linea di comando, senza la mediazione di Fitbit Studio. Per farlo
occorre avere installato Node.js ad una versione 8.x+
1. Lanciare Fitbit OS Simulator
2. Posizionarsi con la propria shell in questa cartella ed eseguire i seguenti comandi:
    npm install //installerà i moduli node.js
    npx fitbit //aprirà una shell fitbit, per rimanere nella shell corrente, eseguire npx-fitbit [COMMAND]
    fitbit$ build
    fitbit$ install
