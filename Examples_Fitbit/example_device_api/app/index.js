import { HeartRateSensor } from "heart-rate";
import document from "document";
import * as messaging from "messaging";

//Display the HR data
const hrmData = document.getElementById("hrm-data");

if(HeartRateSensor){
  const hrm = new HeartRateSensor({ frequency: 1 });
  //On read, we set the HR label value accordingly
  hrm.addEventListener("reading", () => {
    hrmData.text = JSON.stringify({
      //If heart rate sensor can't provide information, set the label to default value of 0
      heartRate: hrm.heartRate ? hrm.heartRate : 0
    });
    //Send data to companion
    if(messaging.peerSocket.readyState === messaging.peerSocket.OPEN){
      messaging.peerSocket.send(hrm.heartRate);
    }
  });
  hrm.start();
} else {
  console.log("Failed to import HeartRateSensor");
}